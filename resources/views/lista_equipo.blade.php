@extends('plantilla')
<style>body {
    font-family: "Open Sans", sans-serif;
    line-height: 1.25;
  }
  
  table {
    border: 1px solid #ccc;
    border-collapse: collapse;
    margin: 0;
    padding: 0;
    width: 100%;
    table-layout: fixed;
  }
  
  table caption {
    font-size: 1.5em;
    margin: .5em 0 .75em;
  }
  
  table tr {
    background-color: #f8f8f8;
    border: 1px solid #ddd;
    padding: .35em;
  }
  
  table th,
  table td {
    padding: .625em;
    text-align: center;
  }
  
  table th {
    font-size: .85em;
    letter-spacing: .1em;
    text-transform: uppercase;
  }
  
  @media screen and (max-width: 600px) {
    table {
      border: 0;
    }
  
    table caption {
      font-size: 1.3em;
    }
    
    table thead {
      border: none;
      clip: rect(0 0 0 0);
      height: 1px;
      margin: -1px;
      overflow: hidden;
      padding: 0;
      position: absolute;
      width: 1px;
    }
    
    table tr {
      border-bottom: 3px solid #ddd;
      display: block;
      margin-bottom: .625em;
    }
    
    table td {
      border-bottom: 1px solid #ddd;
      display: block;
      font-size: .8em;
      text-align: right;
    }
    
    table td::before {
      /*
      * aria-label has no advantage, it won't be read inside a table
      content: attr(aria-label);
      */
      content: attr(data-label);
      float: left;
      font-weight: bold;
      text-transform: uppercase;
    }
    
    table td:last-child {
      border-bottom: 0;
    }
  }
  
  </style>
@section('titulo')
    Bienvenido a Equipos
@endsection

@section('contenido')
    <h1>Lista de Equipos</h1>
    <link rel="stylesheet" href="assets/css/main2.css">
    <nav class="navegacion">
        <a href="{{route('principal')}}">VOLVER A INICIO</a>   
        
    </nav>
    <a href="{{ route('equipos.create')}}">AGREGAR EQUIPOS</a>
    @if(session()->has('mensaje'))
    <p style="color: red">{{ session('mensaje') }}</p>
    @endif
    <table class="container">
        <tr>
            <th>Marca</th>
            <th>Modelo</th>
            <th>Tipo</th>
            <th>Precio</th>
            <th>Referencia</th>
            <th>Editar | ver ficha</th>
            <th>Eliminar</th>
        </tr>
        @foreach ($equipos as $equipo)
            <tr>
                <td>{{ $equipo->marca }}</td>
                <td>{{ $equipo->modelo }}</td>
                <td>{{ $equipo->tipo }}</td>
                <td>{{ $equipo->precio }}</td>
                <td>{{ $equipo->ref }}</td>

                
                <td><a href="{{ route('equipos.show', $equipo->id)}}">Ver ficha</a> 
                    <br>
                    <a href="{{ route('equipos.edit', $equipo->id)}}">Editar</a></td>
                
                <td>
                    <form action="{{ route('equipos.destroy', $equipo->id) }}" method="post">
                        @csrf
                        @method('DELETE')
                        <button type="submit">Eliminar</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
@endsection

