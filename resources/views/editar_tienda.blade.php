<!DOCTYPE html>
<html lang="en">
<title>Editar proveedor</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
.w3-bar,h1,button {font-family: "Montserrat", sans-serif}
.fa-anchor,.fa-coffee {font-size:200px}

input[type=text], select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit] {
  width: 100%;
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

div {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}
</style>
<body>

<!-- Navbar -->

<!-- Header -->
<header class="w3-container w3-red w3-center" style="padding:128px 16px">
  <h1 class="w3-margin w3-jumbo">Editar</h1>
  <p class="w3-xlarge">Editar proveedor</p>
 <a href="{{route('tiendas.index')}}" class="w3-button w3-black w3-padding-large w3-large w3-margin-top" >Atras</a>
</header>

<!-- First Grid -->
<form action="{{ route('tiendas.update', $tienda->id) }}" method="POST">
    @csrf
    @method('PUT')
    <input type="text" name="nombre" placeholder="Ingrese su nombre" value="{{ old('nombre',$tienda->nombre) }}" required/>
    <input type="text" name="dire" placeholder="Ingrese sus apellidos" value="{{ old('dire',$tienda->dire) }}"/>
    <input type="text" name="tel" placeholder="Ingrese su sexo" value="{{ old('tel',$tienda->tel) }}"/>
    <input type="text" name="ref" placeholder="Ingrese su edad" value="{{ old('ref',$tienda->ref) }}"/>
    <button type="submit">Enviar</button>
</form>



<!-- Footer -->
<footer class="w3-container w3-padding-64 w3-center w3-opacity">  
  
</footer>

<script>
// Used to toggle the menu on small screens when clicking on the menu button
function myFunction() {
  var x = document.getElementById("navDemo");
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}
</script>

</body>
</html>
