<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model;

class Equipos extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'equipos';
    protected $fillable=['marca','modelo','tipo','precio','ref'];
}
