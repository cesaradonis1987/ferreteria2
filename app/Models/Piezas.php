<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model;

class Piezas extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'piezas';
    protected $fillable=['marca','modelo','tipo','precio','ref'];
}
