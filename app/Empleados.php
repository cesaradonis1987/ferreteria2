<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model;

class Empleados extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'empleados';
    protected $fillable=['nombre','ape','sex','edad','tel'];
}
