<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model;

class Tiendas extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'tiendas';
    protected $fillable=['nombre','dire','tel','ref'];
}
