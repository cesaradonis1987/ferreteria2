<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model;

class Clientes extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'clientes';
    protected $fillable=['nombre','tipo','ciudad','ref'];
}
